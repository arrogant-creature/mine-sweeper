import MineField from "../domain/MineField";

export default function createListGrid(nOfRows: number, nOfCols: number, minningRatio: number){
	
	function countAdj(grid: MineField[][], y: number, x: number) : number {
		if (grid[y][x].hasMine){
	      return -1;
	    }

	    let count = 0;
	    for (const i of [-1, 0, 1]){
	      	for (const j of [-1, 0, 1]){
	        	if (y +i < 0 || y +i >= nOfRows || x +j < 0 || x +j >= nOfCols){
	          		continue;
		        }
		        if (listGrid[y + i][x + j].hasMine){
			        count++;
			    }
		   	}
	    }

		return count;
		
	}


	let listGrid: MineField[][] = [];



	let fieldCount = -1;
	let numOfMinedFields = 0;


	for (let row = 0; row < nOfRows; row++){
		const r: MineField[] = [];
		for (let col = 0; col < nOfCols; col++){
			fieldCount++;
			let f: MineField = {
			 	isVisited: false,
			 	isLocked: false,
				id: fieldCount,
			 	x: col,
			 	y: row,
			 	hasMine: false,
			 	adjCount: 0
			 };
			 f.hasMine = Math.random() < minningRatio ? true : false;
			 if (f.hasMine){
			 	numOfMinedFields++;
			 }
			 r.push(f);
		}
		listGrid.push(r);
	}

	for (let row = 0; row < nOfRows; row++){
		for (let col = 0; col < nOfCols; col++){
			const n = countAdj(listGrid, row, col);
			listGrid[row][col].adjCount = n;
		}
	}

	return {listGrid, numOfMinedFields}

}