
import './App.css';
import MineSweepGame from './MineSweepGame';

function App() {

  const Howto = () =>
    <div className="Howto" >
      Press 'Esc', 'r' or 'R' to reset game board.
    </div>


  return (
    <div className="App" >
      <Howto />
      <MineSweepGame />
    </div>
  );
}

export default App;
