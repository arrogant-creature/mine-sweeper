export default interface MineField {
    isVisited: boolean,
    isLocked: boolean,
    id: number,
    x: number,
    y: number,
    hasMine: boolean,
    adjCount: number
}