import { useEffect, useState } from 'react';
import Board from './component/board/Board';
import MineField from './domain/MineField';
import createListGrid from './utils/func_createListGrid';

function MineSweepGame() {

  function traverseAndReveal(y: number, x: number, grid: MineField[][], nOfRows: number, nOfCols: number) {

    const nGrid: MineField[][] = JSON.parse(JSON.stringify(grid));

    if (nGrid[y][x].adjCount > 0 || nGrid[y][x].adjCount === -1) {
      nGrid[y][x].isVisited = true;
      setListGrid(nGrid);
      return;
    }

    type Yx = {
      y: number,
      x: number
    }

    const queue: Yx[] = [{ y, x }];

    let loopCount = 0;
    let duplicateCount = 0;

    while (queue.length > 0) {
      console.log("loopCount=" + (++loopCount));
      const ind = queue.shift() as Yx;

      if (nGrid[ind.y][ind.x].isVisited || nGrid[ind.y][ind.x].isLocked) {
        console.log("dupCount" + (++duplicateCount));
        continue;
      }

      // add to queue if adj is empty empty, show number if adj is empty, do nothing if adj is mined
      for (const i of [-1, 0, 1]) {
        for (const j of [-1, 0, 1]) {
          if ((i === 0 && j === 0) || ind.y + i < 0 || ind.y + i >= nOfRows || ind.x + j < 0 || ind.x + j >= nOfCols) {
            continue;
          }

          if (nGrid[ind.y + i][ind.x + j].isVisited || nGrid[ind.y + i][ind.x + j].isLocked) {
            continue;
          }

          if (nGrid[ind.y + i][ind.x + j].adjCount === 0 && (i === 0 || j === 0)) {
            const tmp: Yx = {
              y: ind.y + i,
              x: ind.x + j
            }
            queue.push(tmp);
          } else if (nGrid[ind.y + i][ind.x + j].adjCount > 0) {
            nGrid[ind.y + i][ind.x + j].isVisited = true;
          }
        }
      }
      nGrid[ind.y][ind.x].isVisited = true;
    }

    setListGrid(nGrid);
  }



  const dummy: MineField[][] = []
  const [grid, setListGrid] = useState(dummy);

  const [nOfRows, setNOfRows] = useState(10);
  const [nOfCols, setNOfCols] = useState(10);
  const [minningRatio, setMinningRatio] = useState(0.15);
  const [inGame, setInGame] = useState(true);

  const [remainningMines, setRemainningMines] = useState(-1);
  const [bombCount, setBombCount] = useState(-1);
  const [flagged, setFlagged] = useState(0);

  const [winLose, setWinLose] = useState<'undetermined' | 'Win!' | 'Lost'>('undetermined');

  function handlerKeyDown(event: KeyboardEvent) {

    if (event.key === 'Escape' || event.key === 'r' || event.key === 'R') {
      resetGame()
    }
  }
  const resetGame = () => {
    const tmp = createListGrid(nOfRows, nOfCols, minningRatio);
    const { listGrid, numOfMinedFields } = tmp;
    setListGrid(listGrid);
    setRemainningMines(numOfMinedFields);
    setBombCount(numOfMinedFields);
    setFlagged(0);
    setWinLose('undetermined');
  }


  // useEffect(()=> {
  //   function freshBoard() {
  //     const nBoard = createListGrid(10, 10, 0.15).listGrid;
  //     setListGrid(nBoard);
  //   }
  // }, [])

  useEffect(() => {
    if (remainningMines === 0 && flagged === bombCount) {
      console.log("All Mines Cleared!");
      setWinLose('Win!');
    }
  }, [remainningMines, flagged, bombCount]);

  useEffect(() => {
    const tmp = createListGrid(nOfRows, nOfCols, minningRatio);
    const { listGrid, numOfMinedFields } = tmp;
    setListGrid(listGrid);
    setRemainningMines(numOfMinedFields);
    setBombCount(numOfMinedFields);
  }, [nOfRows, nOfCols, minningRatio]);

  useEffect(() => {
    if (inGame === false) {
      setInGame(true);
    }
  }, [inGame])


  useEffect(() => {
    window.addEventListener('keydown', handlerKeyDown);
    return (() => {
      window.removeEventListener('keydown', handlerKeyDown);
    })
  }, [])


  return (
    <div className="MineSweepGame" >
      <div className="header" >
        {
          winLose === 'undetermined' ? <div> </div> : <div>{winLose}</div>
        }
        <button className="reset" onClick={resetGame}> reset</button>
      </div>
      <Board

        listGrid={grid}

        handleClick={function(y: number, x: number, hasMine: boolean): void {

          traverseAndReveal(y, x, grid, nOfRows, nOfCols);

          if (hasMine) {
            console.log("Clicked on Mine");
            setWinLose('Lost');
            setInGame(false);
          }

        }}
        handleContextMenu={function(y: number, x: number, hasMine: boolean): void {
          const nGrid: MineField[][] = JSON.parse(JSON.stringify(grid));
          if (nGrid[y][x].isLocked) {
            setFlagged(flagged - 1);
            if (hasMine) {
              setRemainningMines(remainningMines + 1);
            }
          } else {
            setFlagged(flagged + 1);
            if (hasMine) {
              setRemainningMines(remainningMines - 1);
            }
          }
          nGrid[y][x].isLocked = !nGrid[y][x].isLocked;
          setListGrid(nGrid);
        }}
      />
    </div>
  );
}

export default MineSweepGame;






