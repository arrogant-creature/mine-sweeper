import MineField from '../../domain/MineField';
import Field from '../field/Field';
import './Board.css';

//, restart: () => void

function Board(props: { listGrid: MineField[][], handleClick: (y: number, x: number, hasMine: boolean) => void, handleContextMenu: (y: number, x: number, hasMine: boolean) => void }) {

  return (
    <div className="Board" onContextMenu={(e) => { e.preventDefault(); }}>
      {
        props.listGrid.map((row, y) =>
          <div className="row" key={y}
            style={{
              gridTemplateColumns: `repeat(${props.listGrid.length}, 1fr`,
              gap: '0',
            }}>
            {
              row.map((field, _x) =>
                <Field
                  model={field}
                  key={field.id}
                  handleClick={props.handleClick}
                  handleContextMenu={props.handleContextMenu}
                />
              )
            }
          </div>
        )
      }

    </div>
  );
}

export default Board;
