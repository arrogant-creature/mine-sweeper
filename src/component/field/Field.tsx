import MineField from '../../domain/MineField';
import './Field.css';

function Field(props: { model: MineField, handleClick: (y: number, x: number, hasMine: boolean) => void, handleContextMenu: (y: number, x: number, hasMine: boolean) => void }) {

    return (
        <div className="Field">
            {
                (props.model.isVisited)
                    ?
                    <div className="container">
                        {props.model.adjCount > 0
                            ? <div className="text">{props.model.adjCount}</div>
                            : props.model.adjCount === -1
                                ? <div className="emoji">💣</div>
                                : <div className="empty">{undefined}</div>}
                    </div>
                    :
                    (props.model.isLocked)
                        ?
                        <div className="container" onContextMenu={() => { props.handleContextMenu(props.model.y, props.model.x, props.model.hasMine); }} >
                            <div className="emoji">🚩</div>
                        </div>

                        :
                        <div className="container"
                            onClick={() => {
                                if (props.model.isLocked) {
                                    return
                                } else {
                                    props.handleClick(props.model.y, props.model.x, props.model.hasMine);
                                }
                            }}
                            onContextMenu={() => {
                                props.handleContextMenu(props.model.y, props.model.x, props.model.hasMine);
                            }}>
                        </div>
            }
        </div>
    );
}

export default Field;
